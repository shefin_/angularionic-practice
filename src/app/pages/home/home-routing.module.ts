import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PickupCallPage } from '../pickup-call/pickup-call.page';

import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage
  },
  {
    path: 'pickup-call',
    component: PickupCallPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule {}
